DRUPAL REFRESH

Simple module which adds delayed page redirect capability via meta-refresh.

Author: Vojtěch Kusý <vojta@atomicant.co.uk>

Copyright: Atomic Ant Ltd. http://atomicant.co.uk

                       .---------------------------.
                  \/  <     Developed by Ants!      >
            _  _  @@  /'---------------------------'
           (_)(_)(_)
           //||||


USAGE
---------

When you attempt the URL with 'refresh' parameter, the site will be refreshed
automatically after default number of seconds. If you want to refresh
the site after specific number of seconds you can specify them in the refresh
parameter. When you add 'destination' parameter, the site will be redirected
to that destination after the specified number of seconds.

E.g. An URL http://domain.com/?refresh will just refresh the page after default
number of seconds, an URL http://domain.com/?destination=node&refresh=5 will
refresh to http://domain.com/node after 5 seconds.

When you call drupal_refresh() directly in the code the rendered page will be
refreshed after default number of seconds. When you call
drupal_refresh(5, 'node') the site will be redirected to http://domain.com/node
after 5 seconds.


API
---------

drupal_refresh($seconds = 0, $path = '', $options = array())

  Companion of drupal_goto().
  Refresh site (using a meta refresh tag) with configurable number of seconds.
  Similar to drupal_goto() but waits for some number of seconds before redirect.

drupal_refresh_get_destination($seconds = 0)

  Companion of drupal_get_destination().
  Prepares a 'destination' and 'refresh' URL query parameters for use in
  combination with drupal_refresh() (see drupal_refresh_init()).
  By default the current URL is returned. If a destination exists in the
  previous request, that destination is returned. As such, a destination can
  persist across multiple pages.
